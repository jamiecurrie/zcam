import UIKit

class ZCAM_Interface: NSObject {
    
    // =======================
    // SETTINGS
    // =======================
    
    let DEBUG               = false
    let FILE_ROOT           = "/DCIM/"
    
    enum CAMERA: String {
        case one            = "http://10.98.32.1"
        case two            = "http://10.98.32.2"
        case three          = "http://10.98.32.3"
        case four           = "http://10.98.32.4"
    }
    
    enum SYSTEM: String {
        case heartbeat      = "/ctrl/session"
        case shutdown       = "/ctrl/shutdown"
        case reboot         = "/ctrl/reboot"
        case info           = "/info"
        case temperature    = "/ctrl/temperature"
    }
    
    enum MODE: String {
        // case playback       = "/ctrl/mode?action=to_pb"
        case capture        = "/ctrl/mode?action=to_cap"
        case record         = "/ctrl/mode?action=to_rec"
    }
    
    enum RECORD: String {
        case start          = "/ctrl/rec?action=start"
        case stop           = "/ctrl/rec?action=stop"
        case remain         = "/ctrl/rec?action=remain"
    }
    
    enum CAPTURE: String {
        case capture        = "/ctrl/still?action=cap"
        case single         = "/ctrl/still?action=single"
        case remain         = "/ctrl/still?action=remain"
        case cancel_burst   = "/ctrl/still?action=cancel_burst"
        case timelapse_stat = "/ctrl/timelapse_stat"
    }
    
    enum FILE_TYPE: String {
        case all            = "?p=1&v=1"
        case videos         = "?v=1"
        case images         = "?p=1"
    }
    
    // =======================
    // HELPERS
    // =======================
    
    typealias completionHandler = (Bool, [String: Any]?) -> Void
    
    func validate(jsonResponse: [String: Any]) -> Bool {
        return (jsonResponse["code"] as! NSNumber == 0)
    }
    
    // =======================
    // SYSTEM
    // =======================
    
    func heartbeat(completion: @escaping completionHandler) {
        requestAsync(url: URL(string: CAMERA.one.rawValue + SYSTEM.heartbeat.rawValue)!) { (success, json) in
            completion(success, json)
        }
    }
    
    func hasCard(camera: CAMERA, completion: @escaping completionHandler) {
        requestAsync(url: URL(string: camera.rawValue + "/ctrl/card?action=present")!) { (success, json) in
            completion(success, json)
        }
    }
    
    @discardableResult func reboot() -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + SYSTEM.reboot.rawValue)!)
    }
    
    @discardableResult func shutdown() -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + SYSTEM.shutdown.rawValue)!)
    }
    
    func info() -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + SYSTEM.info.rawValue)!)
    }
    
    func temperature() -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + SYSTEM.temperature.rawValue)!)
    }
    
    func mode(_ mode: MODE) -> Bool {
        let json = requestSync(url: URL(string: CAMERA.one.rawValue + mode.rawValue)!)
        return validate(jsonResponse: json)
    }
    
    func clearSettings() -> Bool {
        let json = requestSync(url: URL(string: CAMERA.one.rawValue + "/ctrl/set?action=clear")!)
        return validate(jsonResponse: json)
    }
    
    func formatCard(camera: CAMERA) ->  Bool {
        let json = requestSync(url: URL(string: camera.rawValue + "/ctrl/card?action=exfat")!)
        return validate(jsonResponse: json)
    }
    
    
    // =======================
    // CAPTURE
    // =======================
    
    func still(_ action: CAPTURE) -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + action.rawValue)!)
    }
    
    func timelapseStat() -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + CAPTURE.timelapse_stat.rawValue)!)
    }
    
    
    // =======================
    // RECORD
    // =======================
    
    func startRecording() -> Bool {
        
        let info = requestSync(url: URL(string: CAMERA.one.rawValue + "/ctrl/mode")!)
        
        if let mode = info["msg"] as? String {
            if mode != "rec" {
                return false
            }
        }
        return validate(jsonResponse: record(.start))
    }
    
    func stopRecording() -> Bool {
        let info = requestSync(url: URL(string: CAMERA.one.rawValue + "/ctrl/mode")!)
        
        if let mode = info["msg"] as? String {
            if mode != "rec_ing" {
                return false
            }
        }
        return validate(jsonResponse: record(.stop))
    }
    
    func recordTimeRemaining() -> Int {
        let json = record(.remain)
        if let time = json["msg"] as? String {
            return Int(time)!
        }
        return 0
    }
    
    func record(_ action: RECORD) -> [String: Any] {
        return requestSync(url: URL(string: CAMERA.one.rawValue + action.rawValue)!)
    }
    
    
    // =======================
    // SET
    // =======================
    
    func set(key: String, value: String, camera: CAMERA) -> Bool {
        let json = requestSync(url: URL(string: camera.rawValue + "/ctrl/set?" + key + "=" + value)!)
        return validate(jsonResponse: json)
    }
    
    func setDateTime(date: String, time: String) -> Bool {
        // date = YYYY-MM-DD
        // time = hh:mm:ss
        let json = requestSync(url: URL(string: CAMERA.one.rawValue + "/datetime?date=\(date)&time=\(time)")!)
        return validate(jsonResponse: json)
    }
    
    
    // =======================
    // GET
    // =======================
    
    func get(key: String, camera: CAMERA) -> [String: Any] {
        return requestSync(url: URL(string: camera.rawValue + "/ctrl/get?k=" + key)!)
    }
    
    
    // =======================
    // FILE MANGER
    // =======================
    
    func folders(camera: CAMERA) -> Array<String> {
        let json = requestSync(url: URL(string: camera.rawValue + FILE_ROOT)!)
        return json["files"] as! Array<String>
    }
    
    func filesFor(camera: CAMERA, folder: String, filetype: FILE_TYPE) -> Array<String>  {
        let json = requestSync(url: URL(string: camera.rawValue + FILE_ROOT + folder + filetype.rawValue)!)
        return json["files"] as! Array<String>
    }
    
    func fileInfo(camera: CAMERA, folder: String ,file: String) -> [String: Any]  {
        return requestSync(url: URL(string: camera.rawValue + FILE_ROOT + folder + "/" + file + "?act=info")!)
    }
    
    func deleteFile(camera: CAMERA, folder: String ,file: String) -> Bool {
        let json = requestSync(url: URL(string: camera.rawValue + FILE_ROOT + folder + "/" + file + "?act=rm")!)
        return validate(jsonResponse: json)
    }
    
    func fileCreated(camera: CAMERA, folder: String ,file: String) -> Date? {
        let json = requestSync(url: URL(string: camera.rawValue + FILE_ROOT + folder + "/" + file + "?act=ct")!)
        if let time = json["msg"] as? NSNumber {
            return Date(timeIntervalSince1970: TimeInterval(time))
        }
        return nil
    }
    
    func thumbnail(camera: CAMERA, folder: String ,file: String) -> UIImage {
        
        let url = camera.rawValue + FILE_ROOT + folder + "/" + file + "?act=thm"
        if DEBUG { print("👤 Thumbnail:", url) }
        
        do {
            return try UIImage(data: Data(contentsOf: URL(string: url)!))!
        } catch {
            print("❗️ IMAGE decode error")
        }
        return UIImage()
    }
    
    func videoUrl(camera: CAMERA, folder: String, file: String) -> URL {
        
        let fileExt = file.components(separatedBy: "_").last!
        let filePath = FILE_ROOT + folder + "/"
        
        for file in filesFor(camera: camera, folder: folder, filetype: .videos) {
            if file.contains(fileExt) {
                return URL(string: CAMERA.one.rawValue + filePath + file)!
            }
        }
        return URL(string: "about:blank")!
    }
    
    func videoUrlsForAllCameras(folder: String ,file: String) -> [URL] {
        
        let cam1 = videoUrl(camera: .one, folder: folder, file: file)
        let cam2 = videoUrl(camera: .two, folder: folder, file: file)
        let cam3 = videoUrl(camera: .three, folder: folder, file: file)
        let cam4 = videoUrl(camera: .four, folder: folder, file: file)
        
        return [cam1, cam2, cam3, cam4]
    }
    
    
    // =======================
    // REQUESTS
    // =======================
    
    let session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 2
        return URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }()
    
    private func requestSync (url: URL) -> [String: Any] {
        
        if DEBUG { print("\n⬆️ <Sync>", url) }
        
        let semaphore = DispatchSemaphore(value: 0)
        var json: [String: Any] = [:]
        
        session.dataTask(with: url)
        { (data, response, error) in
            
            if (error != nil) { print(error!.localizedDescription); return }
            
            do {
                try json = (JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!
            } catch {
                print("❗️ JSON decode error")
            }
            
            semaphore.signal()
            
            }.resume()
        
        semaphore.wait()
        
        if DEBUG { print("⬇️ JSON:", json) }
        return json
    }
    
    private func requestAsync (url: URL, completion: @escaping completionHandler) {
        
        session.dataTask(with: url) { (data, response, error) in
            
            if self.DEBUG { print("\n⬆️ <Async>", url) }
            
            if (error != nil) { print(error!.localizedDescription); return }
            
            var json: [String: Any]?
            var success = false
            
            do {
                json = try (JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!
                success = self.validate(jsonResponse: json!)
            } catch {
                print("❗️ JSON decode error")
            }
            
            if self.DEBUG { print("⬇️ JSON:", json ?? "") }
            completion(success, json)
            
            }.resume()
        
    }}
