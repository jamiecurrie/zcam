//
//  TestViewController.swift
//  zcam
//
//  Created by Build Machine on 09/02/2017.
//  Copyright © 2017 Immersivevr. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class TestViewController: UIViewController, UIWebViewDelegate {
    
    let zcam: ZCAM_Interface = ZCAM_Interface()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        zcam.heartbeat { (success, json) in
            // code here
        }
        // zcam.reboot()
        // zcam.shutdown()
        // zcam.info()
        // zcam.temperature()
        
        
        // zcam.setDateTime(camera: .one, date: "2016-02-08", time: "16:45:00")
        
        // let folders = zcam.folders(camera: .one)
        // let files = zcam.filesFor(camera: .one, folder: folders[0], filetype: .videos)
        
        // let time = zcam.fileCreated(camera: .one, folder: folders[0], file: files.last!)!
        // print(time)
        
        // let success = zcam.deleteFile(camera: .one, folder: folders[0], file: files[0])
        // print("success:", success)
        
        // thumbnail.image = zcam.getThumbnail(camera: .two, folder: folders[0], file: files[2])
        // thumbnail.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
        
        // zcam.fileInfo(camera: .one, folder: folders[0], file: files[0])
        
        // playVideo(url: zcam.videoUrlsForAllCameras(folder: folders[0], file: files[0]))
        
        // zcam.get(key: "battery", camera: .one)
        // let success = zcam.set(key: "photosize", value: "9M", camera: .one)
        // print("success:", success)
        
        // let success = zcam.mode(.record)
        // print("success:", success)
        
        // let success1 = zcam.startRecording()
        // print("success:", success1)

        // let success2 = zcam.stopRecording()
        // print("success:", success2)
        
        // let remain = zcam.recordTimeRemaining()
        // print("remaining:", remain)
        
        // zcam.clearSettings()
        zcam.hasCard(camera: .one) { (success, json) in
            // code here
        }
        
        // zcam.formatCard(camera: .one)
    }
}




